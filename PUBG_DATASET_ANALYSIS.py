#!/usr/bin/env python
# coding: utf-8

# # Importing Libraries And Forming Dataframes

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns


# In[2]:


df = pd.read_csv("train_V2.csv")


# In[3]:


test = pd.read_csv("test_V2.csv")


# In[4]:


df.info()


# In[5]:


df.isnull()


# In[6]:


df.head()


# In[7]:


df.columns


# In[8]:


mType = pd.get_dummies(df['matchType'])


# In[9]:


mTypeTest = pd.get_dummies(test['matchType'])


# In[10]:


test=pd.concat([test,mTypeTest], axis=1)


# In[11]:


df=pd.concat([df,mType], axis=1)


# In[12]:


df=df.drop(['matchType'],axis=1)


# In[13]:


test=test.drop(['matchType'],axis=1)


# In[14]:


df=df.drop(['Id', 'groupId', 'matchId'],axis=1)


# In[15]:


df.head()


# In[16]:


df['damageDealt'].plot.hist()
plt.axis()
plt.xlabel('Damage Dealt')


# # Working on Train and Test data

# In[17]:


test=test.drop(['Id', 'groupId', 'matchId'],axis=1)


# In[18]:


X_train= df.drop(['winPlacePerc'],axis=1)
y_train= df['winPlacePerc']
X_test= test


# In[19]:


np.where(np.isnan(y_train))


# In[20]:


y_train.iloc[2744604:2744605]


# In[21]:


y_train=y_train.fillna(0.5)


# In[22]:


y_train.iloc[2744604:2744605]


# In[23]:


y_train


# In[24]:


X_test


# In[25]:


X_test.columns


# In[26]:


X_train.columns


# In[27]:


y_train=y_train.values.reshape(-1,1)


# In[28]:


#Fitting Linear Regression to the dataset
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X_train, y_train)


# In[29]:


# Fitting Polynomial Regression to the dataset
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree = 1)
X_poly = poly_reg.fit_transform(X_train)
poly_reg.fit(X_poly, y_train) 


# In[30]:


lin_reg_2 = LinearRegression()
lin_reg_2.fit(X_poly, y_train)


# In[31]:


y_pred=lin_reg_2.predict(poly_reg.fit_transform(X_test))


# # working on prediction

# In[32]:


y_pred.max()


# In[33]:


y_pred.mean()


# In[34]:


y_pred.argmax()


# In[35]:


print (y_pred[147993])


# In[36]:


# PRINT DATA OF PLAYER WITH MAXIMUM WINNING PERCENTAGE
test[147993:147994]


# In[37]:


np.savetxt('winPlacePerc.csv', y_pred, delimiter=',')

